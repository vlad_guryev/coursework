#**The ASP.Net application**

#This application realizes the Caesar cipher decryption and encryption

The list of the application functions includes the following:

*  Read/write *.txt
*  Read/write *.doc, *.docx
*  The encryption/decryption of forementioned types of files using preset cipher *key*
*  The encryption/decryption of text written by user himself
*  The input text field with text from file to display the encrypted text to the user
*  The output text field with decrypted/encrypted text to display the text to the user
*  The "Save as" option to save decrypted/encrypted file to the forementioned types of files on the local machine of the user.
*  The "Save as" option to save decrypted/encrypted text written by user
*  The Frequency Analysis option to decrypt the text and pick a cipher *key*. The analysed *key* is displayed in the key field option of the UI.
The analysed *shift direction* is also displayed in the shift direction option of the UI.
*  Both English and Russian languages are supported
*  Unit tests for doc, txt manager classes and for cryptograph class are realized

Some key features of the realization:

*  The method of decryption/encryption is based on the formula: *encryptedChar = (decryptedChar + key) mod AlphavitLength*
*  The txt reading/writing is realized by the standard *StreamReader* and *System.IO functions*
*  The doc, docx reading/writing is realized by using the GemBox Libriary
*  The txt and doc manager classes inherit the same interface to be convenient in usage for the ProxyModel class
*  The layer between UI and the logic is located in ProxyModel that encapsulates the callings of backend classes
*  The code-behind contains the connection layer between ASP.NET widgets and the EventHandlers
*  The Frequency Analysis is realized by the counting the frequency of the seperate letter in the decrypted text and the
sequential comparison that frequency with the real frequency of Russian or English alphabet letters. The iteration over 
the whole possible key range with the key shift of the decrypted alphabet gives the Dictionary<key, frequency> with
the key and the corresponding delta frequency between all the letters. The key with the minimal delta corresponds to 
the most possible key of the encryption/decryption.