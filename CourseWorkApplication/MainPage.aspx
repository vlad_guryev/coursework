﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainPage.aspx.cs" 
    Inherits="CourseWorkApplication.MainPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Дешифратор. Выполнил Гурьев Влад</title>
    <link rel="stylesheet" href="/Content/Styles.css" />
    <style type="text/css"></style>
</head>
<body>
    <h3>Дешифратор</h3>
    <form id="form1" runat="server">
        <div>
            
            <div> <textarea class ="textArea" name="usertext" id="TextArea1" runat="server"
                placeholder ="Введите текст для дешифрации"></textarea></div>

                <div class="FileUploadArea">
                <h4 class="ChooseFileToUpload">Выберете файл для загрузки (*.txt, *.doc, *.docx):&nbsp;&nbsp;
                <asp:FileUpload class="Button" id="FileUpload1" runat="server" ></asp:FileUpload> </h4>
                    <br /><br />
                </div>

                <div class="MainButtons" >    
                <asp:Button class="Button" type="submit" ID="ReadFileBtn" Runat="server" Text ="Извлечь текст из файла"
                  OnClick="ReadFileHandler"></asp:Button>
                <asp:Button class="Button" type="submit" ID="DecryptBtn" Runat="server"
                   Text="Дешифровать" OnClick="EncryptDecryptHandler"></asp:Button>
                <asp:Button class="Button" type="submit" ID="EncryptBtn" Runat="server"
                   Text="Зашифровать" OnClick="EncryptDecryptHandler"></asp:Button>
                <asp:Button class="Button" type="submit" ID="FreqBtn" Runat="server"
                   Text="Частотный анализ" OnClick="FrequencyAnalysisHandler"></asp:Button>
                </div>

                <div class="Selectors">
                <asp:TextBox class ="ShiftBox" type="number" ID="ShiftTextBox" text="2" Runat="server" ></asp:TextBox>
                <asp:DropDownList class="Selector" id="shiftDirection"  runat="server">
                    <asp:ListItem Text="Влево" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Вправо" Value="1"></asp:ListItem>
                </asp:DropDownList>
                 <asp:DropDownList class="Selector" id="Language"  runat="server">
                    <asp:ListItem Text="Русский" Value="1"></asp:ListItem>
                    <asp:ListItem Text="English" Value="2"></asp:ListItem>
                </asp:DropDownList>    
                </div>

            
                <div class="SaveToFileBtn">
                <asp:RadioButtonList ID="FileTypeList" runat="server" RepeatDirection="Horizontal"
                      CssClass="RBL" TextAlign="Right" BorderColor="#666699" RepeatLayout="Flow" Width="137px">
                    <asp:ListItem Text="TXT" Value="txt" />
                    <asp:ListItem Text="DOCX" Value="docx" />
                </asp:RadioButtonList>
                <asp:Button class="Button" type="submit" ID="WriteFileBtn" Runat="server"
                   Text="Сохранить текст в файл" OnClick="WriteFileHandler" ></asp:Button>
                </div>
            

            <textarea class ="textArea" rows="3" name="usertext" id="TextArea2" runat="server"
                    placeholder =""></textarea>
            
        </div>
    </form>
</body>
</html>
