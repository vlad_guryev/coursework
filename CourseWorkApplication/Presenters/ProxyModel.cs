﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourseWorkApplication.Presenters
{
    using Cryptography;

    public class ProxyModel
    {
        public IFileReader FileManager { get ; set; }
        public string FilePathNew { get { return FileManager.FilePathNew; } }
        public Cryptograph.Language Lang{ get { return Crypter.Lang; } private set { Crypter.Lang = value; } }

        public void SetLang(int index)
        {
            Lang = (Cryptograph.Language)index;
        }

        private Cryptograph Crypter = new Cryptograph();

        public string ReadFromFile(string pathFile)
        {
            return FileManager?.ReadFromFile(pathFile);
        }
        public void WriteToFile(string text, string filePath)
        {
            FileManager.FilePathOriginal = filePath;
            FileManager?.WriteToFile(text);
        }

        public void SetShift(int shift, int direction = 0, int lang = 0)
        {
            Crypter.SetEncryptionOptions(shift, (Cryptograph.Direction)direction,
               (Cryptograph.Language)lang);
        }
        public string Decryption(string areaText)
        {
            return Crypter.Decryption(areaText);
        }

        public string Encryption(string areaText)
        {
            return Crypter.Encryption(areaText);
        }

        public Tuple<string, int, int> FrequencyAnalisys(string text)
        {
            return Crypter.FrequencyAnalisys(text);
        }
    }
}