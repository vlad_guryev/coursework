﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWorkApplication.Presenters
{
    public interface IFileReader
    {
        string FilePathOriginal { get; set; }
        string FilePathNew { get; set; }

        string ReadFromFile(string pathFile);
        void WriteToFile(string text);
    }
}
