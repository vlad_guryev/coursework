﻿using System;
using System.Text;
using System.IO;

namespace CourseWorkApplication.Presenters
{
    public class TxtManager: IFileReader
    {
        public string FilePathOriginal { get; set; }
        public string FilePathNew { get; set; }

        public string ReadFromFile(string pathFile)
        {
            var text = new StringBuilder();
            FilePathOriginal = pathFile;
            FilePathNew = pathFile;
            try
            {
                using (StreamReader sr = File.OpenText(pathFile))
                {
                    string s;
                    while ((s = sr.ReadLine()) != null)
                    {
                        text.Append(s);
                    }
                }
            }
            catch(Exception e)
            {
                text.Append($"Что-то пошло не так при чтении файла, а именно: {e.Message}");
            }

            return text.ToString();
        }

        public void WriteToFile(string createText)
        {
            if (Path.GetExtension(this.FilePathOriginal).Equals(""))
            {
                FilePathNew = this.FilePathOriginal + "\\" + "text_decrypted" + ".txt";
            }
            else
            {
                var path = Path.GetDirectoryName(this.FilePathOriginal);
                var newName = Path.GetFileNameWithoutExtension(this.FilePathOriginal) + "_decrypted";
                FilePathNew = path + "\\" + newName + ".txt"; 
            }

            File.WriteAllText(FilePathNew, createText + Environment.NewLine); 
        }
    }
}