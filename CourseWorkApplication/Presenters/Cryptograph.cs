﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;

namespace CourseWorkApplication.Presenters.Cryptography
{
    public class Cryptograph
    {
        private Dictionary<char, int> Dict;
        private Dictionary<char, int> DictCapit;
        private Dictionary<char, int> DictEncrypted;
        private Dictionary<char, int> DictCapitEncrypted;
        private List<double> realFreq;
        private readonly List<double> RussianFreq =  new List<double>()
        {
            8.01, 1.59, 4.54, 1.70, 2.98, 8.45, 0.04, 0.94, 1.65, 7.35,1.21,
            3.49, 4.40, 3.21, 6.70, 10.97, 2.81, 4.73, 5.47, 6.26, 2.62, 0.26,
            0.97, 0.48, 1.44, 0.73, 0.36, 0.04, 1.90, 1.74, 0.32, 0.64, 2.01
        };
        private readonly List<double> EnglishFreq = new List<double>()
        {
           8.167, 1.492, 2.782, 4.253, 12.702, 2.228, 2.015, 6.094, 6.966,0.153,
           0.772, 4.025, 2.406, 6.749, 7.507, 1.929, 0.095, 5.987, 6.327, 9.056,
           2.758, 0.978, 2.360, 0.150, 1.974, 0.074
        };

        private string russian = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        private string english = "abcdefghijklmnopqrstuvwxyz";

        public string Alphabet
        {
            get
            {
                if (Lang == Language.Russian)
                {
                    return russian;
                }
                else
                {
                    return english;
                }
            }
            private set
            {
                if (Lang == Language.Russian)
                {
                    russian = value;
                }
                else
                {
                    english = value;
                }
            }
        }

        private string AlphCapit => Alphabet.ToUpper();
        private int AlphLength => Alphabet.Length;
        public enum Direction //сдвиг зашифрованного текста по отношению к незашифрованному
        {
            RightShift = 0,
            LeftShift = 1
        }
        public Direction ShiftDirection { get; set; }
        public enum Language
        {
            Russian = 0,
            English = 1
        }
        public Language Lang { get; set; }

        private int key;
        public int Key
        {
            get => key;
            private set
            {
                if (ShiftDirection == Direction.RightShift)
                {
                    this.key = value % AlphLength;
                }
                else if (ShiftDirection == Direction.LeftShift)
                {
                    this.key = AlphLength - value % AlphLength;
                }
            }
        }

        public void SetEncryptionOptions(int key, Direction shift = Direction.LeftShift,
          Language lang = Language.Russian)
        {
            Lang = lang;
            ShiftDirection = shift;
            Key = key;
            InitializeDictionaries();
        }

        public Cryptograph(int key = 2, Direction shift = Direction.LeftShift,
            Language lang = Language.Russian)
        {
            Dict = new Dictionary<char, int>();
            DictCapit = new Dictionary<char, int>();
            DictEncrypted = new Dictionary<char, int>();
            DictCapitEncrypted = new Dictionary<char, int>();
            SetEncryptionOptions(key, shift, lang);
        }

        private void InitializeDictionaries()
        {
            Dict.Clear();
            DictCapit.Clear();
            DictEncrypted.Clear();
            DictCapitEncrypted.Clear();
            //инициализация словарей алфавитов и словарей дешифровки
            for (int i = 0; i < AlphLength; i++)
            {
                Dict.Add(Alphabet.ElementAt(i), i);
                DictCapit.Add(AlphCapit.ElementAt(i), i);
                DictEncrypted.Add(Alphabet.ElementAt((i + Key) % Alphabet.Length), i);
                DictCapitEncrypted.Add(AlphCapit.ElementAt((i + Key) % Alphabet.Length), i);
            }
        }

        private string DecryptToken(string token, int key)
        {
            var res = new StringBuilder();
            for (int i = 0; i < token.Length; i++)
            {
                var curChar = token[i];
                if (Dict.TryGetValue(curChar, out int numberOfCharInAlph))
                {
                    var shiftedIndex = (numberOfCharInAlph + key) % Alphabet.Length;
                    res.Append(Alphabet.ElementAt(shiftedIndex));
                }
                else if (DictCapit.TryGetValue(curChar, out numberOfCharInAlph))
                {
                    var shiftedIndex = (numberOfCharInAlph + key) % AlphCapit.Length;
                    res.Append(AlphCapit.ElementAt(shiftedIndex));
                }
                else
                {
                    res.Append(curChar);
                }
            }
            return res.ToString();
        }

        private string EncryptToken(string token, int key)
        {
            var res = new StringBuilder();
            for (int i = 0; i < token.Length; i++)
            {
                var curChar = token[i];
                if (DictEncrypted.TryGetValue(curChar, out int numberOfCharInAlph))
                {
                    char k = Dict.Where(x => x.Value == numberOfCharInAlph).FirstOrDefault().Key;
                    res.Append(k);
                }
                else if (DictCapitEncrypted.TryGetValue(curChar, out numberOfCharInAlph))
                {
                    char k = DictCapit.Where(x => x.Value == numberOfCharInAlph).FirstOrDefault().Key;
                    res.Append(k);
                }
                else
                {
                    res.Append(curChar);
                }
            }
            return res.ToString();
        }

        public string Encryption(string text)
        {
            return EncryptToken(text, Key);
        }
        public string Decryption(string text)
        {
            return DecryptToken(text, Key);
        }

        //return type: Tuple<decryptedText, direction, key>
        public Tuple<string, int, int> FrequencyAnalisys(string text)
        {
            string originalText = text;

            Dictionary<char, double> CharFreqReal = new Dictionary<char, double>();

            if(Lang == Language.Russian)
            {
                realFreq = RussianFreq;
            }
            else
            {
                realFreq = EnglishFreq;
            }
            InitializeDictionaries();

            for (int i = 0; i < AlphLength; i++)
            {
                CharFreqReal.Add(Alphabet.ElementAt(i), realFreq.ElementAt(i) / 100);
            }
            
            text = text.ToLower();
            text = new String(text.Where(c => c != '-' && (c < '0' || c > '9')).ToArray());
            string[] multiArray = text.Split(new Char[] { ' ', ',', '.', '-', '\n', '\t', '!', ';', '\r', '\\', '#', '@' });
            var sb = new StringBuilder();
            foreach (var item in multiArray)
            {
                sb.Append(item);
            }

            text = sb.ToString();
            var freqChars = text.GroupBy(x => x).Select(x => new { Char = x.Key, Count = x.Count() });

            int summ = 0;
            Dictionary<char, double> CharFreqInText = new Dictionary<char, double>(AlphLength);
            foreach (var item in freqChars)
            {
                if (Dict.ContainsKey(item.Char))
                {
                    Dict[item.Char] = item.Count;
                    summ += item.Count;
                }
            }

            foreach (var item in Dict)
            {
                CharFreqInText[item.Key] = (double)item.Value / summ;
            }

            Dictionary<char, double> ShiftedCharFreqInText = new Dictionary<char, double>(AlphLength);
            Dictionary<int, double> keyFreq = new Dictionary<int, double>(AlphLength);
            double delta = 0;

            for (int key = 0; key < AlphLength; key++)
            {
                for (int i = 0; i < AlphLength; i++)
                {
                    ShiftedCharFreqInText[Dict.ElementAt((i + key) % AlphLength).Key] =
                        CharFreqInText.ElementAt((i + key) % AlphLength).Value;
                }

                for (int i = 0; i < AlphLength; i++)
                {
                    delta += Math.Abs(CharFreqReal.ElementAt(i).Value -
                        ShiftedCharFreqInText.ElementAt(i).Value);
                }
                keyFreq[key] = delta;
                delta = 0;
                ShiftedCharFreqInText.Clear();
            }

            int possibleKey = keyFreq.Aggregate((l, r) => l.Value < r.Value ? l : r).Key;

            this.SetEncryptionOptions(possibleKey, Direction.RightShift, Lang);
            string decryptedText = this.Encryption(originalText);

            char initChar = originalText.ElementAt(0);
            char decryptedChar = decryptedText.ElementAt(0);
            Direction direction = Direction.RightShift;

            if(initChar > decryptedChar)
            {
                direction = Direction.LeftShift;
                possibleKey = AlphLength - possibleKey % AlphLength;
            }

            Tuple<string, int, int> retTuple =
                new Tuple<string, int, int>(decryptedText, 1 - (int)direction, AlphLength - possibleKey);

            return retTuple;
        }
    }
}