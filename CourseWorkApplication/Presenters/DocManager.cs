﻿using System;
using System.Text;
using GemBox.Document;
using System.IO;

namespace CourseWorkApplication.Presenters
{
    public class DocManager : IFileReader
    {
        public string FilePathOriginal { get; set; }
        public string FilePathNew { get; set; }

        static DocManager()
        {
            ComponentInfo.SetLicense("FREE-LIMITED-KEY");
        }

        public string ReadFromFile(string pathFile)
        {
            FilePathOriginal = pathFile;
            FilePathNew = pathFile;
            StringBuilder sb = new StringBuilder();
            try
            {
                DocumentModel document = DocumentModel.Load(pathFile);
                foreach (Paragraph paragraph in document.GetChildElements(true, ElementType.Paragraph))
                {
                    foreach (Run run in paragraph.GetChildElements(true, ElementType.Run))
                    {
                        string text = run.Text;
                        sb.AppendFormat(text);
                    }
                    sb.AppendLine();
                }
            }
            catch (Exception e)
            {
                sb.Append($"Что-то пошло не так при чтении файла, а именно: {e.Message}");
            }

            return sb.ToString();
        }

        public void WriteToFile(string createText)
        {
            if (Path.GetExtension(this.FilePathOriginal).Equals(""))
            {
                FilePathNew = this.FilePathOriginal + "\\" + "text_decrypted" + ".docx";
            }
            else
            {
                var path = Path.GetDirectoryName(this.FilePathOriginal);
                var newName = Path.GetFileNameWithoutExtension(this.FilePathOriginal) + "_decrypted";
                FilePathNew = path + "\\" + newName + ".docx";
            }

            DocumentModel document = new DocumentModel();
            Paragraph paragraph = new Paragraph(document);
            paragraph.Content.LoadText(createText + Environment.NewLine);
            document.Sections.Add(new Section(document, paragraph));
            document.Save(FilePathNew);
        }
    }
}