﻿using System;
using System.Web.UI.WebControls;

namespace CourseWorkApplication
{
    using CourseWorkApplication.Presenters;

    public partial class MainPage : System.Web.UI.Page
    {
        private ProxyModel Model = new ProxyModel();

        protected void Page_Init(object sender, EventArgs e)
        {
            ReadFileBtn.Click += new EventHandler(ReadFileHandler);
            DecryptBtn.Click += new EventHandler(EncryptDecryptHandler);
            EncryptBtn.Click += new EventHandler(EncryptDecryptHandler);
            WriteFileBtn.Click += new EventHandler(WriteFileHandler);
            FreqBtn.Click += new EventHandler(FrequencyAnalysisHandler);
            FileTypeList.SelectedIndex = 1; //default value to DOCX radio btn
        }

        protected void ReadFileHandler(Object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)  
            {
                string fileName = Server.HtmlEncode(FileUpload1.FileName);
                string extension = System.IO.Path.GetExtension(fileName);
                switch (extension)
                {
                    case ".txt":
                        Model.FileManager = new TxtManager();
                        break;
                    case ".doc":
                    case ".docx":
                        Model.FileManager = new DocManager();
                        break;
                    default:
                        TextArea1.InnerText = "Выбран неправильный тип файла";
                        return;
                }
                System.IO.Directory.CreateDirectory(Server.MapPath("~/App_Data/"));
                string localSavedFilePath = Server.MapPath("~/App_Data/" + fileName);
                FileUpload1.SaveAs(localSavedFilePath);
                TextArea1.InnerText = Model.ReadFromFile(localSavedFilePath);
            }
            else
            {
                TextArea1.InnerText = "Вы не выбрали файл";
            }
        }
        
        protected void EncryptDecryptHandler(Object sender, EventArgs e)
        {
            string senderName = (sender as Button).ClientID;

            int.TryParse(ShiftTextBox.Text, out int shift);
            if ((-1 < shift) && (shift < 34))
            {
                if (senderName.Equals(DecryptBtn.ClientID)) 
                {
                    Model.SetShift(shift, 1 - shiftDirection.SelectedIndex, Language.SelectedIndex);
                    TextArea2.InnerText = Model.Decryption(TextArea1.InnerText);
                }
                else
                {
                    Model.SetShift(shift, shiftDirection.SelectedIndex, Language.SelectedIndex);
                    TextArea2.InnerText = Model.Encryption(TextArea1.InnerText);
                }
            }
            else
            {
                TextArea2.InnerText = "Сдвиг должен быть в пределах 0 - 33";
            }
        }

        protected void FrequencyAnalysisHandler(Object sender, EventArgs e)
        {
            if (TextArea1.InnerText.Equals(""))
            {
                return;
            }
            Model.SetLang(Language.SelectedIndex);
            var tuple = Model.FrequencyAnalisys(TextArea1.InnerText);
            TextArea2.InnerText = tuple.Item1;
            shiftDirection.SelectedIndex = tuple.Item2;
            ShiftTextBox.Text = tuple.Item3.ToString();
        }

        protected void WriteFileHandler(Object sender, EventArgs e)
        {
            if (TextArea2.InnerText.Equals(""))
            {
                return;
            }

            if (FileTypeList.SelectedIndex == 0)
            {
                Model.FileManager = new TxtManager();
                Response.ContentType = "text/plain";
            }
            else if(FileTypeList.SelectedIndex == 1)
            {
                Model.FileManager = new DocManager();
                Response.ContentType = "application/msword";
            }
            Model.WriteToFile(TextArea2.InnerText, Server.MapPath("~/App_Data/"));

            Response.AppendHeader("Content-Disposition", $"attachment; " +
                $"filename={System.IO.Path.GetFileName(Model.FilePathNew)}");
            Response.TransmitFile(Model.FilePathNew);
            Response.End();
        }
    }
}