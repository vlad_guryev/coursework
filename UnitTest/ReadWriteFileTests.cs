﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    using CourseWorkApplication.Presenters;

    [TestClass]
    public class ReadWriteFileTests
    {
        [TestMethod]
        public void TestDocReadingWriting()
        {
            DocManager doc = new DocManager();
            doc.FilePathOriginal = @"..\..\TestFiles\test.docx";
            string textToFile = "Он говорил на том изысканном французском языке, на котором не только говорили, но и думали наши деды, и с теми, тихими, покровительственными интонациями, которые свойственны состаревшемуся в свете и при дворе значительному человеку.";
            doc.WriteToFile(textToFile);
            string textFromFile = doc.ReadFromFile(@"..\..\TestFiles\test_decrypted.docx");
            Assert.AreEqual(textToFile.Replace("\r\n",""), textFromFile.Replace("\r\n", ""));
        }
        [TestMethod]
        public void TestTxtReadingWriting()
        {
            TxtManager txt = new TxtManager();
            txt.FilePathOriginal = @"..\..\TestFiles\test.txt";
            string textToFile = "Он говорил на том изысканном французском языке, на котором не только говорили, но и думали наши деды, и с теми, тихими, покровительственными интонациями, которые свойственны состаревшемуся в свете и при дворе значительному человеку.";
            txt.WriteToFile(textToFile);
            string textFromFile = txt.ReadFromFile(@"..\..\TestFiles\test_decrypted.txt");
            Assert.AreEqual(textToFile, textFromFile);
        }
    }
}
