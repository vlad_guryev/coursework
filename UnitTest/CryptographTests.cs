﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    using CourseWorkApplication.Presenters.Cryptography;

    [TestClass]
    public class CryptographTests
    {
        [TestMethod]
        public void TestCryptographEnglishLang()
        {
            Cryptograph cryptor = new Cryptograph(2, Cryptograph.Direction.LeftShift, Cryptograph.Language.English);
            string encrypted;
            string decrypted;
            string textToEncrypt;
            textToEncrypt = "Congratulations";
            encrypted = cryptor.Encryption(textToEncrypt);
            Assert.AreEqual(encrypted, "Eqpitcvwncvkqpu");

            decrypted = cryptor.Decryption(encrypted);
            Assert.AreEqual(decrypted, textToEncrypt);

            cryptor.SetEncryptionOptions(2, Cryptograph.Direction.RightShift, Cryptograph.Language.English);
            textToEncrypt = "Congratulations";
            encrypted = cryptor.Encryption(textToEncrypt);
            Assert.AreEqual(encrypted, "Amlepyrsjyrgmlq");

            decrypted = cryptor.Decryption(encrypted);
            Assert.AreEqual(decrypted, textToEncrypt);
        }
        [TestMethod]
        public void TestCryptographProperties()
        {
            Cryptograph cryptor = new Cryptograph(2, Cryptograph.Direction.LeftShift, Cryptograph.Language.English);
            Assert.AreEqual(cryptor.Alphabet, "abcdefghijklmnopqrstuvwxyz");
            Assert.AreEqual(cryptor.Key, 24); //сдвиг влево на 2 все равно что на право на 24
            cryptor.SetEncryptionOptions(19, Cryptograph.Direction.RightShift, Cryptograph.Language.English);
            Assert.AreEqual(cryptor.Key, 19);

            cryptor.SetEncryptionOptions(12, Cryptograph.Direction.RightShift, Cryptograph.Language.Russian);
            Assert.AreEqual(cryptor.Alphabet, "абвгдеёжзийклмнопрстуфхцчшщъыьэюя");
            Assert.AreEqual(cryptor.Key, 12);
            cryptor.SetEncryptionOptions(19, Cryptograph.Direction.LeftShift, Cryptograph.Language.Russian);
            Assert.AreEqual(cryptor.Key, 14); //сдвиг влево на 19 все равно что направо на 15
        }
        [TestMethod]
        public void TestDefaultCtorOptions()
        {
            Cryptograph cryptor = new Cryptograph();
            string encrypted;
            string decrypted;
            string textToEncrypt;
            encrypted = "Срйётвднба";
            decrypted = cryptor.Decryption(encrypted);
            Assert.AreEqual(decrypted, "Поздравляю");

            textToEncrypt = "Поздравляю";
            encrypted = cryptor.Encryption(textToEncrypt);
            Assert.AreEqual(encrypted, "Срйётвднба");
            
            encrypted = "овъкпв";
            decrypted = cryptor.Decryption(encrypted);
            Assert.AreEqual(decrypted, "машина");

            textToEncrypt = "машина";
            encrypted = cryptor.Encryption(textToEncrypt);
            Assert.AreEqual(encrypted, "овъкпв");
        }
        [TestMethod]
        public void TestCryptographSettingsRightShift()
        {
            Cryptograph cryptor = new Cryptograph();
            cryptor.SetEncryptionOptions(2, Cryptograph.Direction.RightShift, Cryptograph.Language.Russian);
            string encrypted;
            string decrypted;
            string textToEncrypt;

            encrypted = "оюямрю";
            decrypted = cryptor.Decryption(encrypted);
            Assert.AreEqual(decrypted, "работа");

            textToEncrypt = "работа";
            encrypted = cryptor.Encryption(textToEncrypt);
            Assert.AreEqual(encrypted, "оюямрю");

            cryptor.SetEncryptionOptions(29, Cryptograph.Direction.RightShift, Cryptograph.Language.Russian);
            textToEncrypt = "юстиция";
            encrypted = cryptor.Encryption(textToEncrypt);
            Assert.AreEqual(encrypted, "вхцмъмг");
            decrypted = cryptor.Decryption(encrypted);
            Assert.AreEqual(decrypted, textToEncrypt);
        }
        [TestMethod]
        public void TestCryptographSettingsLeftShift()
        {
            Cryptograph cryptor = new Cryptograph();
            cryptor.SetEncryptionOptions(29, Cryptograph.Direction.LeftShift, Cryptograph.Language.Russian);
            string encrypted;
            string decrypted;
            string textToEncrypt;
            textToEncrypt = "юстиция";
            encrypted = cryptor.Encryption(textToEncrypt);
            Assert.AreEqual(encrypted, "ъноетеы");

            decrypted = cryptor.Decryption(encrypted);
            Assert.AreEqual(decrypted, textToEncrypt);
        }
        [TestMethod]
        public void TestCryptographSettingsCapitalized()
        {
            Cryptograph cryptor = new Cryptograph();
            cryptor.SetEncryptionOptions(8, Cryptograph.Direction.RightShift, Cryptograph.Language.Russian);
            string encrypted;
            string decrypted;
            string textToEncrypt;

            textToEncrypt = "юCтИци!9238я";
            encrypted = cryptor.Encryption(textToEncrypt);
            Assert.AreEqual(encrypted, "цCкБоб!9238ч");
            decrypted = cryptor.Decryption(encrypted);
            Assert.AreEqual(decrypted, textToEncrypt);

            cryptor.SetEncryptionOptions(8, Cryptograph.Direction.LeftShift, Cryptograph.Language.Russian);
            textToEncrypt = "юCтИци!9238я";
            encrypted = cryptor.Encryption(textToEncrypt);
            Assert.AreEqual(encrypted, "ёCъРюр!9238ж");
            decrypted = cryptor.Decryption(encrypted);
            Assert.AreEqual(decrypted, textToEncrypt);

            cryptor.SetEncryptionOptions(24, Cryptograph.Direction.RightShift, Cryptograph.Language.Russian);
            textToEncrypt = "АБаЖуР_0%1";
            encrypted = cryptor.Encryption(textToEncrypt);
            Assert.AreEqual(encrypted, "ИЙиПьЩ_0%1");
            decrypted = cryptor.Decryption(encrypted);
            Assert.AreEqual(decrypted, textToEncrypt);

            cryptor.SetEncryptionOptions(24, Cryptograph.Direction.LeftShift, Cryptograph.Language.Russian);
            textToEncrypt = "АБаЖуР_0%1";
            encrypted = cryptor.Encryption(textToEncrypt);
            Assert.AreEqual(encrypted, "ЧШчЮкЗ_0%1");
            decrypted = cryptor.Decryption(encrypted);
            Assert.AreEqual(decrypted, textToEncrypt);
        }
        
    }
}
